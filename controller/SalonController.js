const db = require('./../src/connection');
const config = require('./../src/config');
const { Validator } = require('node-input-validator');
const jwt = require('jsonwebtoken');
// salon list
exports.list = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            db.query('SELECT * FROM salon', function (error, results, fields) {
                if (error)
                    return res.send({ status: 0, msg: error });
                else
                    results.forEach(element => {
                        if (element['salon_profile'] != null) {
                            return element['salon_profile'] = config + element.salon_profile
                        }
                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                        }
                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                        }
                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                        }
                    });
                return res.send({ status: 1, data: results, msg: 'Baber Shop List.' });
            });
        }
    });
}
// salon details
exports.details = (req, res) => {
    const v = new Validator(req.body, {
        salon_id: 'required',
    });
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('SELECT * FROM salon where salon_id=' + req.body.salon_id, async function (error, results, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            results[0]['service'] = [];
                            results[0]['salon_rating'] = [];
                            await db.query('SELECT * FROM salon_service where salon_id=' + req.body.salon_id, async function (errors, service, fields) {
                                if (errors)
                                    return res.send({ status: 0, msg: errors });
                                else {
                                    service.forEach(elements => {
                                        results[0]['service'].push(elements)
                                    })
                                }
                                await db.query('SELECT * FROM rating inner join user on user.user_id = rating.user_id where rating.salon_id=' + req.body.salon_id, function (ratingerror, rating, fields) {
                                    if (ratingerror)
                                        return res.send({ status: 0, msg: ratingerror });
                                    else {
                                        rating.forEach(ratingelement => {
                                            results[0]['salon_rating'].push(ratingelement)
                                        })
                                    }
                                    results.forEach(element => {
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                                        }
                                    });
                                    console.log(results[0]['service']);
                                    return res.send({ status: 1, data: results[0], msg: 'Baber Shop Details.' });
                                });
                            });
                        }
                    });
                }
            });
        }
    });
}
// salon search
exports.search = (req, res) => {
    const v = new Validator(req.body, {
        type: 'required',
        keyword: 'required'
    });
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    if (req.body.type == 1) {
                        db.query('SELECT * FROM salon where salon_zipcode=' + req.body.keyword, function (error, results, fields) {
                            if (error)
                                return res.send({ status: 0, msg: error });
                            else
                                results.forEach(element => {
                                    if (element['salon_profile'] != null) {
                                        return element['salon_profile'] = config + element.salon_profile
                                    }
                                    if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                                        return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                                    }
                                    if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                                        return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                                    }
                                    if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                                        return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                                    }
                                });
                            return res.send({ status: 1, data: results, msg: 'Baber Shop List.' });
                        });
                    }
                    if (req.body.type == 2) {
                        if (req.body.keyword == "less than 3") {
                            keyword = 3
                            db.query('SELECT * FROM salon where rating < ' + keyword, function (error, results, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    results.forEach(element => {
                                        if (element['salon_profile'] != null) {
                                            return element['salon_profile'] = config + element.salon_profile
                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                                        }
                                    });
                                return res.send({ status: 1, data: results, msg: 'Baber Shop List.' });
                            });
                        }
                        if (req.body.keyword == "grater than 3") {
                            keyword = 3
                            db.query('SELECT * FROM salon where rating > ' + keyword, function (error, results, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    results.forEach(element => {
                                        if (element['salon_profile'] != null) {
                                            return element['salon_profile'] = config + element.salon_profile
                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                                        }
                                    });
                                return res.send({ status: 1, data: results, msg: 'Baber Shop List.' });
                            });
                        }
                        if (req.body.keyword == "less than 4") {
                            keyword = 4
                            db.query('SELECT * FROM salon where rating < ' + keyword, function (error, results, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    results.forEach(element => {
                                        if (element['salon_profile'] != null) {
                                            return element['salon_profile'] = config + element.salon_profile
                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                                        }
                                    });
                                return res.send({ status: 1, data: results, msg: 'Baber Shop List.' });
                            });
                        }
                        if (req.body.keyword == "grater than 4") {
                            keyword = 4
                            db.query('SELECT * FROM salon where rating >' + keyword, function (error, results, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    results.forEach(element => {
                                        if (element['salon_profile'] != null) {
                                            return element['salon_profile'] = config + element.salon_profile
                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null && element['salon_gallery_image4'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3, element['salon_gallery_image4'] = config + element.salon_gallery_image4];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null && element['salon_gallery_image3'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2, element['salon_gallery_image3'] = config + element.salon_gallery_image3];

                                        }
                                        if (element['salon_gallery_image1'] != null && element['salon_gallery_image2'] != null) {
                                            return [element['salon_gallery_image1'] = config + element.salon_gallery_image1, element['salon_gallery_image2'] = config + element.salon_gallery_image2];

                                        }
                                    });
                                return res.send({ status: 1, data: results, msg: 'Baber Shop List.' });
                            });
                        }
                    }
                }
            });
        }
    });
}
// rating review
exports.rating = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        rating: 'required',
        review: 'required',
        salon_id: 'required'
    });
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    let salon = { user_id: req.body.user_id, salon_id: req.body.salon_id, rating: req.body.rating, review: req.body.review };
                    db.query("INSERT INTO rating SET ? ", salon, function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            db.query('select count(rating) as rating,SUM(rating) as total from rating where salon_id =' + req.body.salon_id, function (errors, results, fields) {
                                if (errors)
                                    return res.send({ status: 0, msg: errors });
                                else {

                                    var salon_rating = results[0].total / results[0].rating;
                                    let user = { rating: salon_rating };
                                    db.query('UPDATE salon SET ? WHERE salon_id = ?', [user, req.body.salon_id], function (errorsalon, result, fields) {
                                        if (errorsalon)
                                            return res.send({ status: 0, msg: errorsalon });
                                        else {
                                            return res.send({ status: 1, msg: 'Add Rating and review Successful.' });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}