const db = require('../src/connection');
const config = require('../src/config');
const { Validator } = require('node-input-validator');
const jwt = require('jsonwebtoken');
// add user booking
exports.booking = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        booking_date: 'required',
        booking_service: 'required',
        salon_id: 'required'
    });
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    let salon = { user_id: req.body.user_id, salon_id: req.body.salon_id, booking_date: req.body.booking_date, booking_service: req.body.booking_service };
                    db.query("INSERT INTO booking SET ? ", salon, function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {

                            return res.send({ status: 1, msg: 'Booking Appointment Successful.' });

                        }
                    });
                }
            });
        }
    });
}
// user booking appointment list
exports.listuserbooking = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                user_id: 'required',
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from booking inner join salon on salon.salon_id = booking.salon_id  where booking.user_id =' + req.body.user_id + ' order by booking.booking_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['salon_profile'] != null) {
                                    return element['salon_profile'] = config + element.salon_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'User Booking Appointment List.' });

                        }
                    });
                }
            });
        }
    });
}
// user booking appointment search
exports.searchuserbooking = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                user_id: 'required',
                booking_date: 'required'
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from booking inner join salon on salon.salon_id = booking.salon_id  where booking.user_id =' + req.body.user_id + ' and booking.booking_date = "' + req.body.booking_date + '" order by booking.booking_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['salon_profile'] != null) {
                                    return element['salon_profile'] = config + element.salon_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'User Booking Appointment List.' });

                        }
                    });
                }
            });
        }
    });
}
// salon booking appointment list
exports.listsalonbooking = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                salon_id: 'required',
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from booking inner join user on user.user_id = booking.user_id  where booking.salon_id =' + req.body.salon_id + ' order by booking.booking_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['user_profile'] != null) {
                                    return element['user_profile'] = config + element.user_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'Salon Booking Appointment List.' });

                        }
                    });
                }
            });
        }
    });
}
// salon booking appointment search
exports.searchsalonbooking = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                salon_id: 'required',
                booking_date: 'required'
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from booking inner join user on user.user_id = booking.user_id where booking.salon_id =' + req.body.salon_id + ' and booking.booking_date ="' + req.body.booking_date + '" order by booking.booking_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['user_profile'] != null) {
                                    return element['user_profile'] = config + element.user_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'Salon Booking Appointment List.' });

                        }
                    });
                }
            });
        }
    });
}
// salon approve appointment
exports.salonapprovebooking = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                booking_id: 'required'
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from booking where  booking_id =' + req.body.booking_id, function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            let user = { booking_status: 1 };
                            db.query('UPDATE booking SET ? WHERE booking_id = ?', [user, req.body.booking_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    return res.send({ status: 1, msg: 'Salon Approve Booking Appointment Successful.' });
                                }
                            });

                        }
                    });
                }
            });
        }
    });
}

// notifaction
exports.send_notification = async (payload, fcm_id) => {
    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };

    var registrationTokens = [fcm_id];
    notification.messaging().sendToDevice(registrationTokens, payload, options)
        .then((response) => {
            //res.send({ msg: "Successfully sent message:", data: response })
            console.log("Successfully sent message:", response);
        })
        .catch((error) => {
            //res.send({ msg: "Error sending message:", data: error })
            console.log("Error sending message:", error);
        });

};