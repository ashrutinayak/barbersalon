const db = require('../src/connection');
const { Validator } = require('node-input-validator');
const multer = require('multer');
const jwt = require('jsonwebtoken');
const storage = multer.diskStorage({
    destination: function (req, file, cb, error) {
        cb(null, '../public/image');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});
const upload = multer({
    storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 2,
    },
});

exports.imageupload = (req, res) => {
    var filename = {
        'filename': new Date().toISOString() + req.file.originalname.replace(" ", "-")
    };

    return res.send({ status: 1, data: filename, message: 'New image upload successfully.' });
}
exports.about = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            db.query('select * from about', function (error, result, fields) {
                if (error)
                    return res.send({ status: 0, msg: error });
                else {
                    return res.send({ status: 1, data: result, msg: 'About Details.' });

                }
            });
        }
    });
}
