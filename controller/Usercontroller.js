const db = require('./../src/connection');
const config = require('./../src/config');
const md5 = require('md5');
const { Validator } = require('node-input-validator');
var transporter = require('./../src/emailconfig');
const jwt = require('jsonwebtoken');
// signup
exports.signup = (req, res) => {
    const v = new Validator(req.body, {
        user_email: 'required|email',
        user_name: 'required',
        user_password: 'required',
        user_pincode: 'required',
        user_device_id: 'required',
        user_device_type: 'required',
        user_fcm_id: 'required',
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            if (req.body.user_type == 1) {
                db.query('SELECT * FROM user where user_email ="' + req.body.user_email + '"', function (errors, results, fields) {
                    if (errors)
                        return res.send({ status: 0, msg: errors });
                    else
                        if (results == "") {
                            token = Math.floor(100000 + Math.random() * 900000);
                            var email = req.body.user_email;
                            let user = { user_name: req.body.user_name, user_email: req.body.user_email, user_password: md5(req.body.user_password), user_token: token, user_pincode: req.body.user_pincode, user_device_id: req.body.user_device_id, user_device_type: req.body.user_device_type, user_fcm_id: req.body.user_fcm_id };
                            db.query("INSERT INTO user SET ? ", user, function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    var mailOptions = {
                                        from: 'ashrutinayak2697@gmail.com',
                                        to: email,
                                        subject: 'Verification code for barber shop Sign up',
                                        text: 'Your Verification code for barber shop Sign up is:' + token
                                    };
                                transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        return res.send({ status: 1, data: { "user_id": result.insertId }, msg: 'Registre Successful.' });
                                    }
                                });
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'This email id is already used.' });
                        }
                });
            }
            if (req.body.user_type == 2) {
                db.query('SELECT * FROM salon where salon_email ="' + req.body.user_email + '"', function (errors, results, fields) {
                    if (errors)
                        return res.send({ status: 0, msg: errors });
                    else
                        if (results == "") {
                            token = Math.floor(100000 + Math.random() * 900000);
                            var email = req.body.user_email;
                            let salon = { salon_name: req.body.user_name, salon_email: req.body.user_email, salon_password: md5(req.body.user_password), salon_device_id: req.body.user_device_id, salon_token: token, salon_zipcode: req.body.user_pincode, salon_device_type: req.body.user_device_type, salon_fcm_id: req.body.user_fcm_id, salon_address: req.body.salon_address, salon_hours_rate: req.body.salon_hours_rate };
                            db.query("INSERT INTO salon SET ? ", salon, function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    var mailOptions = {
                                        from: 'ashrutinayak2697@gmail.com',
                                        to: email,
                                        subject: 'Verification code for barber shop Sign up',
                                        text: 'Your Verification code for barber shop Sign up is:' + token
                                    };
                                transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        return res.send({ status: 1, data: { "salon_id": result.insertId }, msg: 'Registre Successful.' });
                                    }
                                });
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'This email id is already used.' });
                        }
                });
            }
        }
    });
}
// login function
exports.signin = (req, res) => {
    const v = new Validator(req.body, {
        user_email: 'required|email',
        user_password: 'required',
        user_device_id: 'required',
        user_device_type: 'required',
        user_fcm_id: 'required',
        user_type: 'required'
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            if (req.body.user_type == 1) {
                db.query('SELECT * FROM user where user_email ="' + req.body.user_email + '" and user_password ="' + md5(req.body.user_password) + '"', function (errors, results, fields) {
                    if (errors)
                        return res.send({ status: 0, msg: errors });
                    else
                        if (results != "") {
                            if (results[0].user_status == 0) {
                                return res.send({ status: 2, msg: "Your Account is not verify." });
                            }
                            let user_id = results[0].user_id;
                            let user = { user_device_id: req.body.user_device_id, user_device_type: req.body.user_device_type, user_fcm_id: req.body.user_fcm_id };
                            db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    if (results[0].user_profile != null) {
                                        result = { 'user_id': results[0].user_id, 'user_name': results[0].user_name, 'user_email': results[0].user_email, 'user_pincode': results[0].user_pincode, 'user_profile': config + results[0].user_profile };
                                    }
                                    else {
                                        result = { 'user_id': results[0].user_id, 'user_name': results[0].user_name, 'user_email': results[0].user_email, 'user_pincode': results[0].user_pincode };
                                    }
                                    jwt.sign({ result }, 'secretkey', (err, token) => {
                                        return res.send({ status: 1, data: result, token, msg: 'Login Successful.' });
                                    });
                                }
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
            if (req.body.user_type == 2) {
                db.query('SELECT * FROM salon where salon_email ="' + req.body.user_email + '" and salon_password ="' + md5(req.body.user_password) + '"', function (errors, results, fields) {
                    if (errors)
                        return res.send({ status: 0, msg: errors });
                    else
                        if (results != "") {
                            if (results[0].salon_status == 0) {
                                return res.send({ status: 2, msg: "Your Account is not verify." });
                            }
                            let user_id = results[0].salon_id;
                            let user = { salon_device_id: req.body.user_device_id, salon_device_type: req.body.user_device_type, salon_fcm_id: req.body.user_fcm_id };
                            db.query('UPDATE salon SET ? WHERE salon_id = ?', [user, user_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    if (results[0].salon_profile != null) {
                                        result = { 'salon_id': results[0].salon_id, 'salon_name': results[0].salon_name, 'salon_email': results[0].salon_email, 'salon_zipcode': results[0].salon_zipcode, 'salon_profile': config + results[0].salon_profile };
                                    }
                                    else {
                                        result = { 'salon_id': results[0].salon_id, 'salon_name': results[0].salon_name, 'salon_email': results[0].salon_email, 'salon_zipcode': results[0].salon_zipcode };
                                    }
                                    jwt.sign({ result }, 'secretkey', (err, token) => {
                                        return res.send({ status: 1, data: result, token, msg: 'Login Successful.' });
                                    });
                                }
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
        }
    });
}
// Email verification
exports.emailverify = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        user_token: 'required',
        user_type: 'required'
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            if (req.body.user_type == 1) {
                db.query('SELECT user_id,user_name,user_email,user_token,user_status FROM user where user_id =' + req.body.user_id, function (error, results, fields) {
                    console.log(results[0].user_token);
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            if (results[0].user_token == req.body.user_token) {
                                let user_id = results[0].user_id;
                                let user = { user_status: 1 };
                                db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                                    if (error)
                                        return res.send({ status: 0, msg: error });
                                    else
                                        return res.send({ status: 1, data: results[0], msg: 'Email Verification Successful.' });
                                });
                            }
                            else {
                                return res.send({ status: 2, msg: 'Your verification code is not correct.' });
                            }
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
            if (req.body.user_type == 2) {
                db.query('SELECT salon_id,salon_name,salon_email,salon_token,salon_status FROM salon where salon_id =' + req.body.user_id, function (error, results, fields) {
                    console.log(results[0].salon_token);
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            if (results[0].salon_token == req.body.user_token) {
                                let salon_id = results[0].salon_id;
                                let user = { salon_status: 1 };
                                db.query('UPDATE salon SET ? WHERE salon_id = ?', [user, salon_id], function (error, result, fields) {
                                    if (error)
                                        return res.send({ status: 0, msg: error });
                                    else
                                        return res.send({ status: 1, data: results[0], msg: 'Email Verification Successful.' });
                                });
                            }
                            else {
                                return res.send({ status: 2, msg: 'Your verification code is not correct.' });
                            }
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
        }
    });
}
//ForgetPassword
exports.forgetpassword = (req, res) => {
    const v = new Validator(req.body, {
        user_email: 'required',
        user_type: 'required'
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            if (req.body.user_type == 1) {
                db.query('SELECT user_id,user_name,user_email,user_token FROM user where user_email ="' + req.body.user_email + '"', function (error, results, fields) {
                    token = Math.floor(100000 + Math.random() * 900000);
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            let user_id = results[0].user_id;
                            let email = results[0].user_email;
                            let user = { user_token: token };
                            db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    var mailOptions = {
                                        from: 'ashrutinayak2697@gmail.com',
                                        to: email,
                                        subject: 'Forget password verification code',
                                        text: 'Your Forget password verification code is:' + token
                                    };
                                transporter.sendMail(mailOptions, function (error, info) {
                                    results[0] = { 'user_id': results[0].user_id, 'user_name': results[0].user_name, 'user_email': results[0].user_email, 'user_token': token };
                                    return res.send({ status: 1, data: results[0], msg: 'Verification Code for forget password send in you email address Successful.' });
                                });
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
            if (req.body.user_type == 2) {
                db.query('SELECT salon_id,salon_name,salon_email,salon_token FROM salon where salon_email ="' + req.body.user_email + '"', function (error, results, fields) {
                    token = Math.floor(100000 + Math.random() * 900000);
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            let salon_id = results[0].salon_id;
                            let email = results[0].salon_email;
                            let salon = { salon_token: token };
                            db.query('UPDATE salon SET ? WHERE salon_id = ?', [salon, salon_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else
                                    var mailOptions = {
                                        from: 'ashrutinayak2697@gmail.com',
                                        to: email,
                                        subject: 'Forget password verification code',
                                        text: 'Your Forget password verification code is:' + token
                                    };
                                transporter.sendMail(mailOptions, function (error, info) {
                                    results[0] = { 'salon_id': results[0].salon_id, 'salon_name': results[0].salon_name, 'salon_email': results[0].salon_email, 'salon_token': token };
                                    return res.send({ status: 1, data: results[0], msg: 'Verification code for forget password send in you email address Successful.' });
                                });
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
        }
    });
}
// newpassword set
exports.newpasswordset = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        user_type: 'required',
        user_password: 'required'
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            if (req.body.user_type == 1) {
                db.query('SELECT user_id,user_name,user_email,user_token,user_password FROM user where user_id =' + req.body.user_id, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            let user_id = results[0].user_id;
                            let user = { user_password: md5(req.body.user_password) };
                            db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    results[0] = { 'user_id': results[0].user_id, 'user_name': results[0].user_name, 'user_email': results[0].user_email };
                                    return res.send({ status: 1, data: results[0], msg: 'Your new password set Successful.' });
                                }
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
            if (req.body.user_type == 2) {
                db.query('SELECT salon_id,salon_name,salon_email,salon_token,salon_password FROM salon where salon_id =' + req.body.user_id, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            let salon_id = results[0].salon_id;
                            let salon = { salon_password: md5(req.body.user_password) };
                            db.query('UPDATE salon SET ? WHERE salon_id = ?', [salon, salon_id], function (error, result, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    results[0] = { 'salon_id': results[0].salon_id, 'salon_name': results[0].salon_name, 'salon_email': results[0].salon_email };
                                    return res.send({ status: 1, data: results[0], msg: 'Your new password set Successful.' });
                                };
                            });
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
        }
    });
}
// changepassword
exports.changepassword = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        user_type: 'required',
        user_password: 'required',
        new_password: 'required'
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            if (req.body.user_type == 1) {
                db.query('SELECT user_id,user_name,user_email,user_token,user_password FROM user where user_id =' + req.body.user_id, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            console.log(results[0].user_password)
                            if (results[0].user_password === md5(req.body.user_password)) {
                                if (results[0].user_password === md5(req.body.new_password)) {
                                    return res.send({ status: 0, msg: 'Password is already exits.' });
                                }
                                let user_id = results[0].user_id;
                                let user = { user_password: md5(req.body.new_password) };
                                db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (errors, result, fields) {
                                    if (errors)
                                        return res.send({ status: 0, msg: errors });
                                    else {
                                        return res.send({ status: 1, msg: 'Your new password set Successful.' });
                                    }
                                });
                            }
                            else {
                                return res.send({ status: 0, msg: 'Please Enter Correct Current Password.' });
                            }
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
            if (req.body.user_type == 2) {
                db.query('SELECT salon_id,salon_name,salon_email,salon_token,salon_password FROM salon where salon_id =' + req.body.user_id, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        if (results != "") {
                            if (results[0].salon_password === md5(req.body.user_password)) {
                                if (results[0].salon_password === md5(req.body.new_password)) {
                                    return res.send({ status: 0, msg: 'Password is already exits.' });
                                }
                                let salon_id = results[0].salon_id;
                                let salon = { salon_password: md5(req.body.user_password) };
                                db.query('UPDATE salon SET ? WHERE salon_id = ?', [salon, salon_id], function (errors, result, fields) {
                                    if (errors)
                                        return res.send({ status: 0, msg: errors });
                                    else {
                                        return res.send({ status: 1, msg: 'Your new password set Successful.' });
                                    };
                                });
                            }
                            else {
                                return res.send({ status: 0, msg: 'Please Enter Correct Current Password.' });
                            }
                        }
                        else {
                            return res.send({ status: 2, msg: 'Something Was Wrong.' });
                        }
                });
            }
        }
    });
}
// edit profile
exports.editprofile = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        user_name: 'required',
        user_pincode: 'required',
    });
    v.check().then((matched) => {
        if (!matched) {
            res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
        }
        else {
            db.query('SELECT * FROM user where user_id =' + req.body.user_id, function (error, results, fields) {
                if (error)
                    return res.send({ status: 0, msg: error });
                else
                    if (results != "") {
                        let user_id = results[0].user_id;
                        if (req.body.user_profile != null || req.body.user_profile != "") {
                            var user = { user_name: req.body.user_name, user_pincode: req.body.user_pincode, user_profile: req.body.user_profile }
                        }
                        else {
                            var user = { user_name: req.body.user_name, user_pincode: req.body.user_pincode }
                        }
                        db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (errors, result, fields) {
                            if (errors)
                                return res.send({ status: 0, msg: errors });
                            else {
                                return res.send({ status: 1, msg: 'Update Profile Successful.' });
                            }
                        });
                    }
                    else {
                        return res.send({ status: 2, msg: 'Something Was Wrong.' });
                    }
            });
        }
    });
}