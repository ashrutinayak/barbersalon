const db = require('../src/connection');
const config = require('../src/config');
const notification = require('./appoinmentcontroller')
const { Validator } = require('node-input-validator');
const jwt = require('jsonwebtoken');
// add Chatsenduser
exports.chatsenduser = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        message: 'required',
        salon_id: 'required'
    });
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    let salon = { user_id: req.body.user_id, salon_id: req.body.salon_id, chat_message: req.body.message };
                    db.query("INSERT INTO chat SET ? ", salon, function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            db.query('select * from salon where salon_id = ' + req.body.salon_id, async (salonerror, salonresult, fields) => {
                                if (salonerror)
                                    return res.send({ status: 0, msg: salonerror });
                                else {
                                    db.query('select * from user where user_id = ' + req.body.user_id, async (usererror, userresult, fields) => {
                                        if (usererror)
                                            return res.send({ status: 0, msg: usererror });
                                        else {
                                            var payload = {
                                                notification: {
                                                    title: userresult[0].user_name,
                                                    body: req.body.message,
                                                    noti_type: "0",
                                                },
                                            };
                                            var fcm_id = salonresult[0].salon_fcm_id;
                                            await notification.send_notification(payload, fcm_id)
                                            return res.send({ status: 1, msg: 'Chat Send Successful.' });
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
//  add Chatsendsalon
exports.chatsendsalon = (req, res) => {
    const v = new Validator(req.body, {
        user_id: 'required',
        message: 'required',
        salon_id: 'required'
    });
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    let salon = { user_id: req.body.user_id, salon_id: req.body.salon_id, chat_message: req.body.message };
                    db.query("INSERT INTO chat SET ? ", salon, function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            db.query('select * from user where user_id = ' + req.body.user_id, async (usererror, userresult, fields) => {
                                if (usererror)
                                    return res.send({ status: 0, msg: usererror });
                                else {
                                    db.query('select * from salon where salon_id = ' + req.body.salon_id, async (salonerror, salonresult, fields) => {
                                        if (salonerror)
                                            return res.send({ status: 0, msg: salonerror });
                                        else {
                                            var payload = {
                                                notification: {
                                                    title: salonresult[0].salon_name,
                                                    body: req.body.message,
                                                    noti_type: "0",
                                                },
                                            };
                                            var fcm_id = userresult[0].user_fcm_id;
                                            await notification.send_notification(payload, fcm_id)
                                            return res.send({ status: 1, msg: 'Chat Send Successful.' });
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
// chat list user
exports.chatlistuser = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                user_id: 'required',
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from chat inner join salon on salon.salon_id = chat.salon_id  where chat.user_id =' + req.body.user_id + '  group by chat.salon_id order by chat.chat_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['salon_profile'] != null) {
                                    return element['salon_profile'] = config + element.salon_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'User Chat List.' });

                        }
                    });
                }
            });
        }
    });
}
// chat list salon
exports.chatlistsalon = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                salon_id: 'required',
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from chat inner join user on user.user_id = chat.user_id  where chat.salon_id =' + req.body.salon_id + '  group by chat.user_id order by chat.chat_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['user_profile'] != null) {
                                    return element['user_profile'] = config + element.user_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'Salon Chat List.' });

                        }
                    });
                }
            });
        }
    });
}
// chat list user with salon
exports.chatusersalon = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                user_id: 'required',
                salon_id: 'required'
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from chat inner join salon on salon.salon_id = chat.salon_id  where chat.user_id =' + req.body.user_id + ' and chat.salon_id =' + req.body.salon_id + ' order by chat.chat_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['salon_profile'] != null) {
                                    return element['salon_profile'] = config + element.salon_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'User Chat Details.' });

                        }
                    });
                }
            });
        }
    });
}
// chat list salon with user
exports.chatsalonuser = (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.send({ status: 0, msg: "Your token is not working." });
        }
        else {
            const v = new Validator(req.body, {
                user_id: 'required',
                salon_id: 'required'
            });
            v.check().then((matched) => {
                if (!matched) {
                    res.send({ status: 0, msg: "Please enter all required fields.", data: v.errors });
                }
                else {
                    db.query('select * from chat inner join user on user.user_id = chat.user_id  where chat.salon_id =' + req.body.salon_id + ' and chat.user_id =' + req.body.user_id + ' order by chat.chat_id DESC', function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            result.forEach(element => {
                                if (element['user_profile'] != null) {
                                    return element['user_profile'] = config + element.user_profile
                                }
                            })
                            return res.send({ status: 1, data: result, msg: 'Salon Chat Details.' });

                        }
                    });
                }
            });
        }
    });
}