var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
var router = require('./route');
const Router = require('./route');
app.use('/public/image', express.static('public/image'));

app.use('/', Router);
// set port
app.listen(4000, () => {
    console.log('Node app is running on port 4000');
});

module.exports = app;