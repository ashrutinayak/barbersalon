var express = require('express');
var varification = require('./verify');
const Router = express.Router();
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb, error) {
        cb(null, 'public/image/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const upload = multer({
    storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 2,
    },
});
const UserController = require('../controller/Usercontroller')
const SalonController = require('../controller/SalonController')
const AppointmentController = require('../controller/appoinmentcontroller')
const ImageUploadController = require('../controller/ImageuploadController')
const ChatController = require('../controller/chatcontroller')
Router.post("/signup", UserController.signup)
Router.post("/signin", UserController.signin)
Router.post("/emailverification", UserController.emailverify)
Router.post("/forgetpassword", UserController.forgetpassword)
Router.post("/newpassword", UserController.newpasswordset)
Router.get('/list', varification, SalonController.list)
Router.post('/salondetails', varification, SalonController.details)
Router.post('/salonsearch', varification, SalonController.search)
Router.post('/addrating', varification, SalonController.rating)
Router.post('/addbooking', varification, AppointmentController.booking)
Router.post('/userbooking', varification, AppointmentController.listuserbooking)
Router.post('/searchbookinguser', varification, AppointmentController.searchuserbooking)
Router.post('/salonbooking', varification, AppointmentController.listsalonbooking)
Router.post('/searchbookingsalon', varification, AppointmentController.searchsalonbooking)
Router.post('/salonapprovebooking', varification, AppointmentController.salonapprovebooking)
Router.post('/imageupload', upload.single('profile'), ImageUploadController.imageupload)
Router.get('/about', varification, ImageUploadController.about)
Router.post('/changepassword', varification, UserController.changepassword)
Router.post('/editprofileuser', varification, UserController.editprofile)
Router.post('/chatsenduser', varification, ChatController.chatsenduser)
Router.post('/chatsendsalon', varification, ChatController.chatsendsalon)
Router.post('/chatlistuser', varification, ChatController.chatlistuser)
Router.post('/chatlistsalon', varification, ChatController.chatlistsalon)
Router.post('/chatusersalon', varification, ChatController.chatusersalon)
Router.post('/chatsalonuser', varification, ChatController.chatsalonuser)

module.exports = Router;